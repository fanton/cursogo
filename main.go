package main

import (
	"net/http"
	"log"
	"github.com/gorilla/mux"
)

func main(){
	router := mux.NewRouter()

	server := http.ListenAndServe(":8080", router)
	log.Fatal(server)
}