package main

type Course struct {
	Name string 	`json:"name"`
	Level int 		`json:"level"`
	Teacher string `json:"teacher"`
}

type Courses []Course
