# Ejercicio: Desarrollo de una API en Go

El presente proyecto corresponde al ejercicio planteado como parte del curso de créditos en la FCAD UNER.

## Introducción

Estas instrucciones le permitirán obtener una copia del proyecto para el funcionamiento en su máquina local para fines de evaluación y prueba.

### Requisitos previos

* Go
* MongoDB
* Conector de Go para MongoDB: https://gopkg.in/mgo.v2
* Librería para Rutas: https://github.com/gorilla/mux

### Instalación

A continuación se detallan los pasos a seguir para ejecutar el proyecto en un entorno de desarrollo o testing.

```
Descargar el repositorio en ZIP o clonarlo desde la consola
```

## Ejecución

Para ejecutar el proyecto debe compiar todos los archivos go desde la consola:

```
go run *.go
```

### Uso

A continuación se explican los comandos disponibles y sus características.

```
/cursos
```


## Autor

* **Federico Antón**

## Licencia

Este proyecto se encuentra bajo licencia GPLv3 - ver el archivo [LICENSE.md](LICENSE.md) para más detalles.
