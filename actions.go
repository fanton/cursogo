package main

import (
	"fmt"
	"net/http"
	"github.com/gorilla/mux"
	"encoding/json"
	"log"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func getSession() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost")

	if err != nil{
		panic(err)
	}

	return session
}

func responseCourse(w http.ResponseWriter, status int, results Course){
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)
}

func responseCourses(w http.ResponseWriter, status int, results []Course){
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)
}

var collection = getSession().DB("curso_go").C("courses")

func Index(w http.ResponseWriter, r *http.Request){
		fmt.Fprintf(w, "Hola mundo desde mi servidor web con GO")
}

func CourseList(w http.ResponseWriter, r *http.Request){
		var results []Course
		err := collection.Find(nil).Sort("-_id").All(&results)

		if err != nil {
			log.Fatal(err)
		}else{
			fmt.Println("Resultados: ", results)
		}

		responseCourses(w, 200, results)
}

func CourseShow(w http.ResponseWriter, r *http.Request){
		params := mux.Vars(r)
		course_id := params["id"]

		if !bson.IsObjectIdHex(course_id) {
			w.WriteHeader(404)
			return
		}

		oid := bson.ObjectIdHex(course_id)

		results := Course{}
		err := collection.FindId(oid).One(&results)
		
		if err != nil{
			w.WriteHeader(404)
			return
		}

		responseCourse(w, 200, results)
}

func CourseAdd(w http.ResponseWriter, r *http.Request){
	decoder := json.NewDecoder(r.Body)

	var course_data Course 
	err := decoder.Decode(&course_data)

	if(err != nil){
		panic(err)
	}

	defer r.Body.Close()

	err = collection.Insert(course_data)

	if err != nil{
		w.WriteHeader(500)
		return
	}

	responseCourse(w, 200, course_data)
}

func CourseUpdate(w http.ResponseWriter, r *http.Request){
		params := mux.Vars(r)
		course_id := params["id"]

		if !bson.IsObjectIdHex(course_id) {
			w.WriteHeader(404)
			return
		}

		oid := bson.ObjectIdHex(course_id)
		
		decoder := json.NewDecoder(r.Body)

		var course_data Course
		err := decoder.Decode(&course_data)

		if err != nil {
			panic(err)
			w.WriteHeader(500)
			return
		}

		defer r.Body.Close()

		document := bson.M{"_id": oid}
		change := bson.M{"$set": course_data}
		err = collection.Update(document, change)

		if err != nil {
			w.WriteHeader(404)
			return
		}
		
		responseCourse(w, 200, course_data)
}

type Message struct {
	Status string `json:"status"`
	Message string `json:"message"`
}

func (this *Message) setStatus(data string){
	this.Status = data
}

func (this *Message) setMessage(data string){
	this.Message = data
}

func CourseRemove(w http.ResponseWriter, r *http.Request){
		params := mux.Vars(r)
		course_id := params["id"]

		if !bson.IsObjectIdHex(course_id) {
			w.WriteHeader(404)
			return
		}

		oid := bson.ObjectIdHex(course_id)

		err := collection.RemoveId(oid)
		if err != nil{
			w.WriteHeader(404)
			return
		}

		message := new(Message)

		message.setStatus("success")
		message.setMessage("El curso ID "+course_id+" ha sido borrado correctamente")

		results := message
		w.Header().Set("Content-Type","application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(results)
}